import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TabsPage } from "./tabs.page";
import { AuthGuard } from "./../shared/services/auth.guard";

const routes: Routes = [
  {
    path: "mkt",
    component: TabsPage,
    children: [
      {
        path: "tab1",
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../tab1/tab1.module").then(m => m.Tab1PageModule),
            canActivate: [AuthGuard]
          }
        ]
      },
      {
        path: "tab2",
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../tab2/tab2.module").then(m => m.Tab2PageModule),
            canActivate: [AuthGuard]
          }
        ]
      },
      {
        path: "tab3",
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../tab3/tab3.module").then(m => m.Tab3PageModule),
            canActivate: [AuthGuard]
          }
        ]
      },
      {
        path: "tab4",
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../tab4/tab4.module").then(m => m.Tab4PageModule)
          }
        ]
      },
      {
        path: "",
        redirectTo: "/mkt/tab4",
        pathMatch: "full"
      }
    ]
  },
  {
    path: "",
    redirectTo: "/mkt/tab4",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
