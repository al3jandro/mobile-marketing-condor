import { Router } from "@angular/router";
import { AlertController } from "@ionic/angular";
import { ApiService } from "./../shared/services/api.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-tab4",
  templateUrl: "./tab4.page.html",
  styleUrls: ["./tab4.page.scss"]
})
export class Tab4Page implements OnInit {
  email: string;
  senha: string;
  logged: boolean;
  constructor(
    private api: ApiService,
    public alert: AlertController,
    private router: Router
  ) {
    this.logged = true;
  }

  ngOnInit() {
    if (this.api.isLoggedIn()) {
      this.logged = false;
      this.router.navigate(["mkt/tab1"]);
    }
  }

  onLogin() {
    const rows = {
      email: this.email,
      password: this.senha
    };
    this.api.signIn(rows).subscribe(
      data => {
        localStorage.setItem("access", JSON.stringify(data));
        this.router.navigate(["mkt/campanha"]);
        this.logged = false;
      },
      err => {
        this.presentAlert("Erro", "Usuário ou Senha Invalida.");
      }
    );
  }

  onLogout() {
    return this.api.signOut().subscribe(data => {
      localStorage.removeItem("access");
      this.presentAlert("Alert", "Tchauuuu.");
      this.router.navigate([""]);
      this.logged = true;
    });
  }
  async presentAlert(header: string, message: string) {
    const alert = await this.alert.create({
      header,
      message,
      buttons: ["OK"]
    });
    await alert.present();
  }
}
