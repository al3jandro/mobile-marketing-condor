import { CooperadasPage } from "./../shared/page/cooperadas/cooperadas.page";
import { IonicModule } from "@ionic/angular";
import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Tab3Page } from "./tab3.page";
import { AuthGuard } from "./../shared/services/auth.guard";

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      { path: "", component: Tab3Page },
      {
        path: "cooperadas",
        loadChildren: () =>
          import("../shared/page/cooperadas/cooperadas.module").then(
            m => m.CooperadasPageModule
          ),
        canActivate: [AuthGuard]
      }
    ])
  ],
  declarations: [Tab3Page]
})
export class Tab3PageModule {}
