import { Component, OnInit } from "@angular/core";
import { ApiService } from "../shared/services/api.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-tab3",
  templateUrl: "tab3.page.html",
  styleUrls: ["tab3.page.scss"]
})
export class Tab3Page implements OnInit {
  cooperadas: any = [];
  constructor(private api: ApiService, private router: Router) {}

  ngOnInit() {
    this.api.getCooperadas().subscribe(data => {
      this.cooperadas = data;
    });
  }

  doRefresh(event) {
    console.log(event);
    this.api.getCooperadas().subscribe(data => {
      this.cooperadas = data;
    });
    setTimeout(() => {
      event.target.complete();
    }, 1000);
  }

  getCooperada(slug: string, name: string) {
    return this.router.navigate(["mkt/tab3/cooperadas", slug, name]);
  }
}
