import { IonicModule } from "@ionic/angular";
import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Tab1Page } from "./tab1.page";
import { AuthGuard } from "../shared/services/auth.guard";

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      { path: "", component: Tab1Page },
      {
        path: "produtos",
        loadChildren: () =>
          import("../shared/page/products/products.module").then(
            m => m.ProductsPageModule
          ),
        canActivate: [AuthGuard]
      }
    ])
  ],
  declarations: [Tab1Page]
})
export class Tab1PageModule {}
