import { Router } from "@angular/router";
import { ApiService } from "./../shared/services/api.service";
import { Component, OnInit } from "@angular/core";
import { ActionSheetController, AlertController } from "@ionic/angular";

@Component({
  selector: "app-tab1",
  templateUrl: "tab1.page.html",
  styleUrls: ["tab1.page.scss"]
})
export class Tab1Page implements OnInit {
  campanhas: any = [];
  constructor(
    public actionSheetController: ActionSheetController,
    public alert: AlertController,
    private api: ApiService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getCampanha();
  }

  doRefresh(event) {
    this.getCampanha();
    setTimeout(() => {
      event.target.complete();
    }, 1000);
  }
  getCampanha() {
    this.api.getCampanhas().subscribe(data => {
      this.campanhas = data;
    });
  }

  async deleteCamapanha(id: any) {
    const alert = await this.alert.create({
      header: "Info!",
      message: `
        Vaí apagar a Campanha?
      `,
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          cssClass: "secondary",
          handler: () => {
            console.log("Confirm Cancelar: blah");
          }
        },
        {
          text: "Apagar",
          handler: () => {
            this.api.delete("Campanhas", id).subscribe(data => {
              console.log(data);
            });
          }
        }
      ]
    });

    await alert.present();
  }

  async actionCampanha(id: number) {
    const actionSheet = await this.actionSheetController.create({
      buttons: [
        {
          text: "Ver Campanha",
          icon: "eye",
          handler: () => {
            this.router.navigate(["mkt/tab1/produtos", id]);
          }
        },
        {
          text: "Apagar Campanha",
          role: "destructive",
          icon: "trash",
          handler: () => {
            this.deleteCamapanha(id);
            console.log("Delete clicked", id);
          }
        },
        {
          text: "Cancelar",
          icon: "close",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });
    await actionSheet.present();
  }
}
