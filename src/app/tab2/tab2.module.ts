import { IonicModule } from "@ionic/angular";
import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Tab2Page } from "./tab2.page";
import { AuthGuard } from "../shared/services/auth.guard";

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      { path: "", component: Tab2Page },
      {
        path: "produto",
        loadChildren: () =>
          import("../shared/page/product/product.module").then(
            m => m.ProductPageModule
          ),
        canActivate: [AuthGuard]
      }
    ])
  ],
  declarations: [Tab2Page]
})
export class Tab2PageModule {}
