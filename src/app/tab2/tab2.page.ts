import { ApiService } from "./../shared/services/api.service";
import { Component } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-tab2",
  templateUrl: "tab2.page.html",
  styleUrls: ["tab2.page.scss"]
})
export class Tab2Page {
  items: any = [];
  precoRede: any;
  precoSC: any;
  precoNorte: any;
  constructor(private api: ApiService, private router: Router) {}

  search(event: any) {
    const value = event.target.value;
    this.api.getSearch(value).subscribe(data => {
      this.items = data;
      data.forEach(el => {
        const x = el.data.lst_preco_regiao;
        console.log(x);
        x.forEach(t => {
          console.log(t.cod_regiao);
          console.log(t.vlr_preco_regular);
          if (t.cod_regiao === 2) {
            this.precoRede = t.vlr_preco_regular;
            console.log(t.vlr_preco_regular);
          }
          if (t.cod_regiao === 3) {
            this.precoSC = t.vlr_preco_regular;
            console.log(t.vlr_preco_regular);
          } else {
            this.precoNorte = t.vlr_preco_regular;
            console.log(t.vlr_preco_regular);
          }
        });
      });
    });
  }
  getProduct(id: number) {
    this.router.navigate(["mkt/tab2/produto", id]);
  }
}
