import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { ApiService } from "./api.service";

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private api: ApiService, private router: Router) {}

  canActivate() {
    // Verifica se existe Token
    if (this.api.isLoggedIn()) {
      // Caso exista token retorna true
      return true;
    } else {
      // Caso não exista envia para página de cpf não é cadastrado
      this.router.navigate([""]);
      return false;
    }
  }
}
