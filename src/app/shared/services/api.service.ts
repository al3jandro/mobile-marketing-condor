import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { environment } from "./../../../environments/environment.prod";

@Injectable({
  providedIn: "root"
})
export class ApiService {
  url: string = environment.api.url;
  token: string = environment.api.token;
  constructor(private http: HttpClient) {}

  getCampanhas(): Observable<any[]> {
    return this.http
      .get<any[]>(`${this.url}/Campanhas?filter[where][status]=1`)
      .pipe(tap(data => data));
  }

  getFindCampanhaStatus(
    field: string,
    search: string,
    status: number = 1
  ): Observable<any[]> {
    return this.http
      .get<any[]>(
        `${this.url}/Campanhas?filter[where][${field}]=${search}&filter[where][status]=${status}`
      )
      .pipe(tap(data => data));
  }

  // Search Produtos

  getSearch(name: string) {
    const filter = `filter[where][dsc_produto][like]=%25${name}%25`;
    return this.http
      .get<any[]>(`${this.url}/Produtos?${filter}&access_token=${this.token}`)
      .pipe(tap(data => data));
  }

  // Produtos X Campanha
  getFindProductXCampanha(id: number): Observable<any[]> {
    return this.http
      .get<any[]>(
        `${this.url}/Produtos?filter[where][campanhaId]=${id}&access_token=${this.token}`
      )
      .pipe(tap(data => data));
  }

  getProduct(id: number): Observable<any[]> {
    return this.http
      .get<any[]>(`${this.url}/Produtos/${id}&access_token=${this.token}`)
      .pipe(tap(data => data));
  }

  getCooperadas() {
    return this.http
      .get<any[]>(
        `${this.url}/Cooperadas/groupCooperada?access_token=${this.token}`
      )
      .pipe(tap(data => data));
  }

  getCooperada(slug: string) {
    return this.http
      .get<any[]>(
        `${this.url}/Cooperadas?filter[where][slug]=${slug}&filter[order]=ordem%20ASC&access_token=${this.token}`
      )
      .pipe(tap(data => data));
  }

  // Update
  update(table: string, id: number, data: any) {
    const headers = new HttpHeaders({
      "Content-Type": "application/json; charset=utf-8"
    });
    return this.http.put<any[]>(`${this.url}/${table}/${id}`, data, {
      headers
    });
  }
  // Delete
  delete(table: string, id: string): Observable<any[]> {
    const headers = new HttpHeaders({
      "Content-Type": "application/json; charset=utf-8"
    });
    return this.http.delete<any[]>(`${this.url}/${table}/${id}`, { headers });
  }

  /**
   * Sign-In
   */
  signIn(data: any) {
    const headers = new HttpHeaders({
      "Content-Type": "application/json; charset=utf-8"
    });
    return this.http.post(`${this.url}/Customers/login`, data, { headers });
  }

  /**
   * Sign-Out
   */
  signOut() {
    const accessToken = this.getAccessToken();
    return this.http.post(
      `${this.url}/Customers/logout?access_token=${accessToken}`,
      {}
    );
  }

  /**
   * LocalStorage
   */
  isLoggedIn() {
    return localStorage.getItem("access") ? true : false;
  }

  getAccessToken() {
    return JSON.parse(localStorage.getItem("access")).id;
  }

  getAccessTokenUserId() {
    return JSON.parse(localStorage.getItem("access")).userId;
  }
}
