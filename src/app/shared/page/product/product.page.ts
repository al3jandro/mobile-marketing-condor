import { ActionSheetController, AlertController } from "@ionic/angular";
import { ApiService } from "./../../services/api.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-product",
  templateUrl: "./product.page.html",
  styleUrls: ["./product.page.scss"]
})
export class ProductPage implements OnInit {
  public id: number;
  item: any = [];
  constructor(
    private activate: ActivatedRoute,
    private api: ApiService,
    private router: Router,
    public actionSheetController: ActionSheetController,
    public alert: AlertController
  ) {
    this.activate.url.subscribe(u => {
      this.id = this.activate.snapshot.params.id;
      this.getProduct(this.id);
    });
  }

  ngOnInit() {}

  getProduct(id: number) {
    this.api.getProduct(id).subscribe(data => {
      this.item = data;
    });
  }

  async deleteProduto(id: any) {
    const alert = await this.alert.create({
      header: "Info!",
      message: `
        Vaí apagar o Produto?
      `,
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          cssClass: "secondary",
          handler: () => {
            console.log("Confirm Cancelar: blah");
          }
        },
        {
          text: "Apagar",
          handler: () => {
            this.api.delete("Produtos", id).subscribe(data => {
              console.log(data);
            });
          }
        }
      ]
    });

    await alert.present();
  }

  async actionCampanha(id: number) {
    const actionSheet = await this.actionSheetController.create({
      buttons: [
        {
          text: "Editar Produto",
          icon: "eye",
          handler: () => {
            this.presentAlert("Info", "De aqui a pouco vai funcionar!");
          }
        },
        {
          text: "Apagar Produto",
          role: "destructive",
          icon: "trash",
          handler: () => {
            this.deleteProduto(id);
            console.log("Delete clicked", id);
          }
        },
        {
          text: "Cancelar",
          icon: "close",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });
    await actionSheet.present();
  }

  async presentAlert(header: string, message: string) {
    const alert = await this.alert.create({
      header,
      message,
      buttons: ["OK"]
    });
    await alert.present();
  }
}
