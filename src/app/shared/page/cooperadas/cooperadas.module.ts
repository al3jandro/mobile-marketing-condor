import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { CooperadasPage } from "./cooperadas.page";

const routes: Routes = [
  {
    path: ":id/:name",
    component: CooperadasPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CooperadasPage]
})
export class CooperadasPageModule {}
