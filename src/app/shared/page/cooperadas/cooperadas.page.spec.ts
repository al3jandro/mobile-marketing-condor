import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CooperadasPage } from './cooperadas.page';

describe('CooperadasPage', () => {
  let component: CooperadasPage;
  let fixture: ComponentFixture<CooperadasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CooperadasPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CooperadasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
