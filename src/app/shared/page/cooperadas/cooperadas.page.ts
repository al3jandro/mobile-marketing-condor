import { ApiService } from "./../../services/api.service";
import { ActivatedRoute } from "@angular/router";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-cooperadas",
  templateUrl: "./cooperadas.page.html",
  styleUrls: ["./cooperadas.page.scss"]
})
export class CooperadasPage implements OnInit {
  public id: string;
  public name: string;
  items: any = [];
  peperoni: any;
  constructor(private activate: ActivatedRoute, private api: ApiService) {
    this.activate.url.subscribe(u => {
      this.id = this.activate.snapshot.params.id;
      this.name = this.activate.snapshot.params.name;
      this.api.getCooperada(this.id).subscribe(data => {
        this.items = data;
      });
    });
  }

  ngOnInit() {}
  doRefresh(event) {
    this.api.getCooperada(this.id).subscribe(data => {
      this.items = data;
    });

    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }
  selectChange(item: any) {
    const rows = {
      action: item.action,
      block: item.block,
      name: item.name,
      ordem: item.ordem,
      slug: item.slug
    };
    this.api.update("Cooperadas", item.id, rows).subscribe(data => {});
  }
}
