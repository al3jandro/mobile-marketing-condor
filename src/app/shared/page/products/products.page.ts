import { ApiService } from "./../../services/api.service";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-products",
  templateUrl: "./products.page.html",
  styleUrls: ["./products.page.scss"]
})
export class ProductsPage implements OnInit {
  items: any = [];
  public searchTerm: string;
  public id: number;
  constructor(
    private api: ApiService,
    private activate: ActivatedRoute,
    private router: Router
  ) {
    this.activate.url.subscribe(u => {
      this.id = this.activate.snapshot.params.id;
      console.log(this.id);
      this.getProducts(this.id);
    });
  }

  ngOnInit() {}

  getProducts(id: number) {
    this.api.getFindProductXCampanha(id).subscribe(data => {
      this.items = data;
    });
  }

  getProduct(id: number) {
    this.router.navigate(["mkt/tab2/produto", id]);
  }

  doRefresh(event) {
    this.getProducts(this.id);

    setTimeout(() => {
      console.log("Async operation has ended");
      event.target.complete();
    }, 2000);
  }
  loadData(event) {}

  search(event: any) {
    const filter = this.items.filter(rows => {
      // tslint:disable-next-line:semicolon
      return rows.dsc_produto.toLowerCase().indexOf(this.searchTerm) >= 0;
    });
    this.items = filter;
  }

  filterItems = (needle, heystack) => {
    const query = needle.toLowerCase();
    return heystack.filter(item => item.toLowerCase().indexOf(query) >= 0);
  };
}
